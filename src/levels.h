/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

void load_level(snake_t * s){
	sys_memset(s->palya, 0, PALYA_HEIGHT*PALYA_WIDTH*3);
	koord x, y;
	switch(s->level){
		case 1://keret
			for(x=0; x<PALYA_WIDTH; x++){
				s->palya[x][0][type] = FAL;
				s->palya[x][PALYA_HEIGHT-1][type] = FAL;
			}
			for(y=1; y<PALYA_HEIGHT; y++){
				s->palya[0][y][type] = FAL;
				s->palya[PALYA_WIDTH-1][y][type] = FAL;
			}
			s->fej[X] = 32; s->fej[Y] = 15;
			s->n_ures = 2*(PALYA_HEIGHT+PALYA_WIDTH)-4;
			break;
		case 2://tunnel
			for(x=0; x<8; x++){
				s->palya[x][0][type] = FAL;
				s->palya[x][PALYA_HEIGHT-1][type] = FAL;
				s->palya[PALYA_WIDTH-1-x][0][type] = FAL;
				s->palya[PALYA_WIDTH-1-x][PALYA_HEIGHT-1][type] = FAL;
			}
			for(y=1; y<8; y++){
				s->palya[0][y][type] = FAL;
				s->palya[PALYA_WIDTH-1][y][type] = FAL;
				s->palya[0][PALYA_HEIGHT-1-y][type] = FAL;
				s->palya[PALYA_WIDTH-1][PALYA_HEIGHT-1-y][type] = FAL;
			}
			for(x=10; x<PALYA_WIDTH-10; x++){
				s->palya[x][PALYA_HEIGHT/2-4][type] = FAL;
				s->palya[x][PALYA_HEIGHT/2+4][type] = FAL;
			}
			s->fej[X] = 32; s->fej[Y] = 15;
			s->n_ures = 4*8+4*7+2*(PALYA_WIDTH-20);
			break;
		case 3://spiral
			for(x=0; x<PALYA_WIDTH/2+4; x++){
				s->palya[x][PALYA_HEIGHT/3-1][type] = FAL;
				s->palya[PALYA_WIDTH-1-x][PALYA_HEIGHT*2/3+1][type] = FAL;
			}
			for(y=0; y<PALYA_HEIGHT/2+1; y++){
				s->palya[PALYA_WIDTH/3][PALYA_HEIGHT-1-y][type] = FAL;
				s->palya[PALYA_WIDTH*2/3][y][type] = FAL;
			}
			s->fej[X] = 27; s->fej[Y] = 19;
			s->n_ures = 2*(PALYA_WIDTH/2+4) + 2*(PALYA_HEIGHT/2+1);
			break;
		case 4://blockade
			for(x=0; x<PALYA_WIDTH; x++){
				s->palya[x][0][type] = FAL;
				s->palya[x][PALYA_HEIGHT-1][type] = FAL;
			}
			for(y=1; y<PALYA_HEIGHT*2/5; y++){
				s->palya[0][y][type] = FAL;
				s->palya[PALYA_WIDTH-1][y][type] = FAL;
				s->palya[0][PALYA_HEIGHT-1-y][type] = FAL;
				s->palya[PALYA_WIDTH-1][PALYA_HEIGHT-1-y][type] = FAL;
			}
			for(y=PALYA_HEIGHT*4/23; y<PALYA_HEIGHT*10/23; y++){
				s->palya[PALYA_WIDTH*5/21][y][type] = FAL;
				s->palya[PALYA_WIDTH*15/21][y][type] = FAL;
				s->palya[PALYA_WIDTH*5/21][PALYA_HEIGHT-1-y][type] = FAL;
				s->palya[PALYA_WIDTH*15/21][PALYA_HEIGHT-1-y][type] = FAL;
			}
			for(x=PALYA_WIDTH/3-2; x<PALYA_WIDTH*2/3; x++){
				s->palya[x][PALYA_HEIGHT/3-1][type] = FAL;
				s->palya[x][PALYA_HEIGHT-1-PALYA_HEIGHT/3+1][type] = FAL;
			}
			
			s->fej[X] = 32; s->fej[Y] = 15;
			s->n_ures = 2*PALYA_WIDTH+4*(PALYA_HEIGHT*2/5-1)+4*((PALYA_HEIGHT*10/23)-(PALYA_HEIGHT*4/23))+2*((PALYA_WIDTH*2/3)-(PALYA_WIDTH/3-2));
			break;
		case 5://twisted
			for(x=0; x<PALYA_WIDTH; x++){
				s->palya[x][0][type] = FAL;
				s->palya[x][PALYA_HEIGHT*9/23][type] = FAL;
				s->palya[x][PALYA_HEIGHT*14/23][type] = FAL;
			}
			for(y=PALYA_HEIGHT*14/23+1; y<PALYA_HEIGHT; y++){
				s->palya[PALYA_WIDTH/2][y][type] = FAL;
			}
			for(y=1; y<PALYA_HEIGHT*9/23; y++){
				s->palya[PALYA_WIDTH/3][y][type] = FAL;
			}
			for(x=PALYA_WIDTH/3+1; x<PALYA_WIDTH/2; x++){
				s->palya[x][PALYA_HEIGHT*9/23][type] = URES;
			}
			for(x=PALYA_WIDTH*2/21; x<PALYA_WIDTH*5/21; x++){
				s->palya[x][0][type] = URES;
			}
			for(y=1; y<PALYA_HEIGHT*3/23; y++){
				s->palya[0][y][type] = FAL;
			}
			s->fej[X] = 32; s->fej[Y] = 15;
			s->n_ures = 3*PALYA_WIDTH + (PALYA_HEIGHT-(PALYA_HEIGHT*14/23+1)) + (PALYA_HEIGHT*9/23-1) - ((PALYA_WIDTH/2)-(PALYA_WIDTH/3+1)) - ((PALYA_WIDTH*5/21)-(PALYA_WIDTH*2/21)) + ((PALYA_HEIGHT*3/23)-1);
			break;
		case 0://üres
		default:
			s->fej[X] = 32; s->fej[Y] = 15;
			s->n_ures = 0;
			break;
	}
}

/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define PRINTX(xx) (18*(xx)-18)
#define PRINTY(yy) (24*(yy))

void CopySpriteNbit(const unsigned char* data, int x, int y, int width, int height, color_t* palette, unsigned int bitwidth) {
	color_t* VRAM = (color_t*)0xA8000000;
	VRAM += (LCD_WIDTH_PX*y + x);
	int offset = 0;
	unsigned char buf;
	for(int j=y; j<y+height; j++) {
		int availbits = 0;
		for(int i=x; i<x+width;  i++) {
			if (!availbits) {
				buf = data[offset++];
				availbits = 8;
			}
			color_t this = ((color_t)buf>>(8-bitwidth));
			*VRAM = palette[(color_t)this];
			VRAM++;
			buf<<=bitwidth;
			availbits-=bitwidth;
		}
		VRAM += (LCD_WIDTH_PX-width);
	}
}

unsigned short rekordok[10][10];
unsigned char rekordok_page = 0;

void rekord_init(){
	sys_memset(rekordok, 0, sizeof(rekordok));
}

void wait_exit(){
	int key=0;
	while(key!=KEY_CTRL_EXIT){
		GetKey(&key);
	}
}

unsigned char szamjegyek(unsigned short szam){
	unsigned int i=10;
	unsigned char j=1;
	while(szam>=i){
		i=i*10;
		j++;
	}
	return j;
}

void highscores(char * message, unsigned char hely){
	MsgBoxPush(6);
	PrintXY(3, 2, message-2, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	
	int i;
	int x=40;	
	int y=61;
	char buffer[20];
	int slen;
	for(i=0; i<=9; i++){
		itoa(i+1,buffer);
		slen=strlen(buffer); buffer[slen]='.'; buffer[slen+1]=0;
		x=40+(i>=5)*130;
		PrintMini(&x, &y, buffer, (i==hely)*0x04, 0xFFFFFFFF, 0, 0, COLOR_BLACK, COLOR_WHITE, 1, 0);
		itoa(rekordok[rekordok_page][i],buffer);
		x=90+(i>=5)*130;
		PrintMini(&x, &y, buffer, (i==hely)*0x04, 0xFFFFFFFF, 0, 0, COLOR_BLACK, COLOR_WHITE, 1, 0);
		if(i==4){y=61;}else{y+=18;}
	}
	
	if(hely<=10){
		buffer[2] = (rekordok_page<6)? 'L' : 'C'; buffer[3] = '1' + rekordok_page%6; buffer[4] = 0;
		PrintXY(16, 4, buffer, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		wait_exit();
		MsgBoxPop();
	}else{
		const color_t fkey_palette[2] = {COLOR_WHITE, COLOR_BLACK};
		int result;
		GetFKeyPtr(0x03E0, &result);//RESET
		PrintCXY(PRINTX(14)+3, PRINTY(6), "F5", TEXT_MODE_NORMAL, -1, COLOR_BLACK, COLOR_WHITE, 1, 0 );
		PrintCXY(PRINTX(16)-2, PRINTY(6), ":", 0x20, -1, COLOR_BLACK, COLOR_WHITE, 1, 0 );
		CopySpriteNbit(result, PRINTX(17)-4, PRINTY(7), 64, 22, fkey_palette, 1);
		
		buffer[2] = 0xE6; buffer[3] = 0x9A; buffer[4] = (rekordok_page<6)? 'L' : 'C'; buffer[5] = '1' + rekordok_page%6; buffer[6] = 0xE6; buffer[7] = 0x9B; buffer[8] = 0;
		PrintXY(15, 4, buffer, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	
		int key=0;
		while(key!=KEY_CTRL_EXIT && key!=KEY_CTRL_RIGHT && key!=KEY_CTRL_LEFT && key!=KEY_CTRL_F5){
			GetKey(&key);
		}
		if(key==KEY_CTRL_F5){
			if(new_game_dialog()){
				for(i=0; i<10; i++){
					rekordok[rekordok_page][i] = 0;
					save_rekordok();
				}
				MsgBoxPop();
				highscores("   HIGH SCORES", 255);
			}else{
				MsgBoxPop();
				highscores("   HIGH SCORES", 255);
			}
		}else if(key==KEY_CTRL_LEFT || key== KEY_CTRL_RIGHT){
			MsgBoxPop();
			if(key==KEY_CTRL_RIGHT){rekordok_page=(rekordok_page+1)%10;}
			if(key==KEY_CTRL_LEFT){ rekordok_page=(rekordok_page+9)%10;}
				highscores("   HIGH SCORES", 255);
		}else{
			MsgBoxPop();
		}
	}
}

void end_game(snake_t s){
	game_mode = end;
	print_status(s.pont);
	save_game(s);

	unsigned char hely=10;
	while(s.pont>rekordok[s.level][hely-1] && hely>=1){
		hely--;
	}
	if(hely<=9){
		//új rekord
		//sys_memmove(rekordok+(hely+1)*sizeof(short), rekordok+hely*sizeof(short), (9-hely)*sizeof(short));
		unsigned char i;
		for(i=9;i>hely;i--){
			rekordok[s.level][i]=rekordok[s.level][i-1];
		}
		rekordok[s.level][hely]=s.pont;
		save_rekordok();
		rekordok_page = s.level;
		highscores("   New record!", hely);
	}else{
		MsgBoxPush(6);
		PrintXY(6, 2, "xxGame over!", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXY(3, 4, "xxYour score is:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		char buffer[]="xx00000";
		itoa(s.pont, buffer+7-szamjegyek(s.pont));
		PrintXY(15, 5, buffer, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXY_2(TEXT_MODE_NORMAL, 1, 7, 2, TEXT_COLOR_BLACK);
		wait_exit();
		MsgBoxPop();
	}
}



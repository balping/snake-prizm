/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

enum {jatek, paused, end} game_mode = end;

char selected_level = 0;
char level_select_menu();

char new_game_dialog(){
	MsgBoxPush(4);
	PrintXY(3, 2, "xxAre you sure?", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY_2(TEXT_MODE_NORMAL, 1, 4, 3, TEXT_COLOR_BLACK);
	PrintXY_2(TEXT_MODE_NORMAL, 1, 5, 4, TEXT_COLOR_BLACK);
	int key;
	char vissza=2;
	do{
		GetKey(&key);
		switch(key){
			case KEY_CTRL_F1:
				vissza = 1;
				break;
			case KEY_CTRL_F6:
			case KEY_CTRL_EXIT:
				vissza = 0;
				break;
		}
	}while(vissza==2);
	MsgBoxPop();
	return vissza;
}

char menu(){
	MsgBoxPush(6);
	PrintXY(7, 2, "xxMAIN MENU", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	char selected=(game_mode == paused);
	char pause = (game_mode == paused);
	char megy=1;
	int key;
	char vissza = 0;
	OS_InnerWait_ms(200);
	while(megy){
		PrintXY(3, 3, "New game"-2, selected==0, TEXT_COLOR_BLACK);
		if(game_mode==pause){
			PrintXY(12, 3, "Continue"-2, selected==1, TEXT_COLOR_BLACK);
		}
		PrintXY(3, 4, "Setup"-2, selected==2, TEXT_COLOR_BLACK);
		PrintXY(3, 5, "High scores"-2, selected==3, TEXT_COLOR_BLACK);
		PrintXY(3, 6, "Level editor"-2, selected==4, TEXT_COLOR_BLACK);
		PrintXY(3, 7, "Help / About"-2, selected==5, TEXT_COLOR_BLACK);

		GetKey(&key);
		switch(key){
			case KEY_CTRL_RIGHT:
			case KEY_CTRL_DOWN:
				selected=(selected+1)%6;
				if(!pause && selected==1){selected=2;}
				break;
			case KEY_CTRL_LEFT:
			case KEY_CTRL_UP:
				selected=(selected+5)%6;
				if(!pause && selected==1){selected=0;}
				break;
			case KEY_CTRL_EXE:
				switch(selected){
					case 0:
						if(game_mode==jatek || game_mode==paused){
							if(new_game_dialog()){
								vissza = 1;
							}
						}else{
							vissza = 1;
						}
						if(vissza==1){
							vissza=level_select_menu();
							if(vissza>=0){
								selected_level = vissza;
								vissza = 1;
								megy=0;
							}else{
								vissza=0;
							}
						}
						break;
					case 1:
						vissza = 2;
						megy=0;
						break;
					case 2:
						setup();
						break;
					case 3:
						highscores("   HIGH SCORES", 255);
						break;
					case 4:
						break;
					case 5:
						help();
						break;
				}
				break;
			case KEY_CTRL_F1:
				help();
				break;
			case KEY_CTRL_F2:
			case KEY_CTRL_SETUP:
				setup();
				break;
		}
	}
	
	MsgBoxPop();
	return vissza;
}

char level_select_menu(){
	MsgBoxPush(6);
	PrintXY(6, 2, "xxSELECT LEVEL", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	char selected=0;
	char select_elozo=0;
	char megy=1;
	int key;
	char vissza = 0;
	char buff[] = "xxL0";
	
	snake_t prev = new_snake(0);
	print_level_prev(prev);
	
	PrintXY(18, 7, "xx\xE6\x9A\xE6\x9B", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);

	while(megy){
		if(selected<5){
			PrintXY(3, 3, "xxEmpty   ", selected==0, TEXT_COLOR_BLACK);
			PrintXY(3, 4, "xxBox     ", selected==1, TEXT_COLOR_BLACK);
			PrintXY(3, 5, "xxTunnel  ", selected==2, TEXT_COLOR_BLACK);
			PrintXY(3, 6, "xxSpiral  ", selected==3, TEXT_COLOR_BLACK);
			PrintXY(3, 7, "xxBlockade", selected==4, TEXT_COLOR_BLACK);
		}else{
			PrintXY(3, 3, "xxTwisted ", selected==5, TEXT_COLOR_BLACK);
			PrintXY(3, 4, "xxCustom 1", selected==6, TEXT_COLOR_BLACK);
			PrintXY(3, 5, "xxCustom 2", selected==7, TEXT_COLOR_BLACK);
			PrintXY(3, 6, "xxCustom 3", selected==8, TEXT_COLOR_BLACK);
			PrintXY(3, 7, "xxCustom 4", selected==9, TEXT_COLOR_BLACK);
		}
		
		buff[2] = (selected<6)? 'L' : 'C';
		buff[3] = '1' + selected%6;
		PrintXY(14, 7, buff, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		
		select_elozo = selected;
		GetKey(&key);
		switch(key){
			case KEY_CTRL_RIGHT:
			case KEY_CTRL_LEFT:
				selected = (selected<5)? 5 : 0;
				break;
			case KEY_CTRL_DOWN:
				selected=(selected+1)%10;
				break;
			case KEY_CTRL_UP:
				selected=(selected+9)%10;
				break;
			case KEY_CTRL_EXE:
				megy = 0;
				break;
			case KEY_CTRL_EXIT:
				selected=-1;
				megy=0;
				break;
		}
		if(select_elozo!=selected && selected>=0){
				prev.level = selected;
				init_snake(&prev, selected);
				print_level_prev(prev);
		}
	}
	
	MsgBoxPop();
	return selected;
}

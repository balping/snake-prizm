/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fxcg/display.h>
#include <color.h>
#include <display_syscalls.h>
#include <disp_tools.hpp>
#include <keyboard.h>
#include <keyboard.hpp>
#include <string.h>
#include <sprite.h>
#include <stdlib.h>
#include <fxcg.h>
#include "snake.h"
#include "levels.h"
#include "level_prev.h"
#include "setup.h"
#include "menu.h"
#include "highscore.h"
#include "file.h"
#include "help.h"

int PRGM_GetKey(void){
  unsigned char buffer[12];
  PRGM_GetKey_OS( buffer );
  return ( buffer[1] & 0x0F ) * 10 + ( ( buffer[2] & 0xF0 ) >> 4 );
}

#define menu_elemez() ({\
	switch(menu()){\
		case 1:\
			goto new_game;\
			break;\
		case 2:\
			goto folytatas;\
			break;\
	}\
})



void print_status(unsigned short score){
	char status_message[] = "Snake                ";
	if(game_mode==jatek){
		memcpy(status_message+6, "- Playing ", 9);
	}else if(game_mode==paused){
		memcpy(status_message+6, "- Paused ", 8);
	}
	DefineStatusMessage(status_message,1,0,0);
	DisplayStatusArea();
	char buffer[] = "00000";
	itoa(score, buffer+5-szamjegyek(score));
	int x=320;
	int y=3;
	PrintMini(&x, &y, buffer, 0x40, 0xFFFFFFFF, 0, 0, COLOR_BLACK, COLOR_WHITE, 1, 0);
}



int main(void) {
	Bdisp_AllClr_VRAM();
	Bdisp_EnableColor(1);
	DrawFrame(COLOR_WHITE);
	EnableStatusArea(0);
	DefineStatusAreaFlags(DSA_SETDEFAULT, 0, 0, 0);
	DefineStatusAreaFlags(3, SAF_BATTERY | SAF_TEXT | SAF_GLYPH | SAF_ALPHA_SHIFT, 0, 0);

	load_settings();

	int key, lastkey;
	irany menet;
	esemeny e;
	int RTC, RTCx, dRTC;
	char running;
	char varakozas=0;
	koord new_kaja_x, new_kaja_y;
	snake_t snake;
	
	init_snake(&snake, 0);
	load_game(&snake);
	rekord_init();
	load_rekordok();
	
eleje:
	print_status(snake.pont);
	Bdisp_PutDisp_DD();
	menu_elemez();
new_game:
	init_snake(&snake, selected_level);
	new_kaja(&snake, &new_kaja_x, &new_kaja_y);
	print_snake(snake);
	Bdisp_PutDisp_DD();
folytatas:
	running=1;
	game_mode = jatek;
	print_snake(snake);
	print_status(snake.pont);
	Bdisp_PutDisp_DD();
	while(running) {
		key=0;
		lastkey=0;
		RTC = RTC_GetTicks();
		do{
			key = PRGM_GetKey();
			//if(key == KEY_PRGM_EXIT || key == GOMB_FEL || key == GOMB_LE || key == GOMB_BALRA || key == GOMB_JOBBRA){
			if(key!=0){
				if(key == GOMB_FEL || key == GOMB_LE || key == GOMB_BALRA || key == GOMB_JOBBRA){
					lastkey=key;
					varakozas=0;
				}else{
					switch(key){
						case KEY_PRGM_EXIT:
						case KEY_PRGM_MENU:
							varakozas=1;
							game_mode = paused;
							save_game(snake);
							print_status(snake.pont);
							menu_elemez();
							break;
						case KEY_PRGM_F1:
							varakozas=1;
							game_mode = paused;
							save_game(snake);
							print_status(snake.pont);
							help();
							game_mode = jatek;
							print_status(snake.pont);
							break;
						case KEY_PRGM_F2:
							varakozas=1;
							game_mode = paused;
							save_game(snake);
							print_status(snake.pont);
							setup();
							game_mode = jatek;
							print_status(snake.pont);
							break;
					}
				}
			}
			OS_InnerWait_ms(7);
			RTCx=RTC_GetTicks();
			if(RTCx<RTC){dRTC=11059200-RTC+RTCx;}else{dRTC=RTCx-RTC;}
			dRTC = dRTC*1000/128;
		}while(dRTC<KOR_MS || varakozas);
		
		/*switch lastkey*/
		if(lastkey == GOMB_FEL){menet=fel;}
		else if(lastkey == GOMB_LE){menet=le;}
		else if(lastkey == GOMB_JOBBRA){menet=jobbra;}
		else if(lastkey == GOMB_BALRA){menet=balra;}
		else{menet=snake.menet;}
		/*end of switch lastkey*/
				
		if(irany_inverz[menet] == snake.menet){menet=snake.menet;}
		snake.menet = menet;
		e = detect_esemeny(snake, menet);

	
		switch(e){
			case lepes:
				lep(&snake);
				print_lepes(snake);
				break;
			case kaja:
				insert_before_head(&snake);
				new_kaja(&snake, &new_kaja_x, &new_kaja_y);
				print_cell(snake,new_kaja_x,new_kaja_y);
				print_lepes(snake);
				snake.pont++;
				print_status(snake.pont);
				break;
			case utkozes:
				end_game(snake);
				goto eleje;
				break;
		}
		Bdisp_PutDisp_DD();
		
	}

	return 0;
}

/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define draw_cell_prev(x,y,color)(fillArea((x)*2+200, (y)*2+100, 2, 2, (color)))
#define print_cell_prev(s,x,y)({\
	switch((s).palya[(x)][(y)][type]){\
		case FAROK:\
			draw_cell_prev((x), (y), SZIN_FAROK);\
			break;\
		case FEJ:\
			draw_cell_prev((x), (y), SZIN_FEJ);\
			break;\
		case SIMA:\
			draw_cell_prev((x), (y), SZIN_SIMA);\
			break;\
		case KAJA:\
			draw_cell_prev((x), (y), SZIN_KAJA);\
			break;\
		case FAL:\
			draw_cell_prev((x), (y), SZIN_FAL);\
			break;\
		default:\
			draw_cell_prev((x), (y), COLOR_LIGHTBLUE);\
			break;\
	}\
})

void print_level_prev(snake_t s){
	koord x, y;
	for(y=0; y<PALYA_HEIGHT; y++){
		for(x=0; x<PALYA_WIDTH; x++){
			print_cell_prev(s, x, y);
		}
	}
}

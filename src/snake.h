/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define version 102

#define URES 0
#define FEJ 1
#define FAROK 2
#define SIMA 3
#define KAJA 4
#define FAL 5

#define PALYA_WIDTH 63
#define PALYA_HEIGHT 32
#define PADDING_PX 1
#define CELL_SIZE_PX 5
#define PALYA_START_X 3
#define PALYA_START_Y 25//space+24
#define KOR_JEL 128 //hányszor 1/128s egy kör

#define X 0
#define Y 1

#define n_irany 0
#define p_irany 1
#define type 2

typedef enum {utkozes, kaja, lepes} esemeny;
typedef enum {fel, le, jobbra, balra, nincs} irany;
char irany_matrix[4][2] = {{0, PALYA_HEIGHT-1}, {0, 1}, {1, 0}, {PALYA_WIDTH-1, 0}};
irany irany_inverz[4] = {le, fel, balra, jobbra};
typedef unsigned short koord;

//palya szerkezet: 7-6: következő elem iránya; 5-4: előző elem iránya; 3-0: típus;
//#define set_mezo(tipus,elozo,kovetkezo)(((unsigned char)(((((kovetkezo)<<2)|(elozo))<<4)|(tipus))))


typedef struct{
	koord fej[2];
	koord farok[2];
	unsigned short hossz;
	unsigned short pont;
	irany menet;
	unsigned char level;
	unsigned short n_ures;
	unsigned char palya[PALYA_WIDTH][PALYA_HEIGHT][3];
} snake_t;

snake_t new_snake(unsigned char level);
void init_snake(snake_t * n, unsigned char level);
void insert_before_head(snake_t * s);
void print_snake(snake_t s);
//void draw_cell(int x, int y, color_t color);
esemeny detect_esemeny(snake_t s, irany i);
void lep(snake_t * s);
void new_kaja(snake_t * s, koord * px, koord * py);
unsigned short random(int extra_seed);
//void print_cell(snake_t s, koord x, koord y);
void print_lepes(snake_t s);

#define draw_cell(x,y,color)(fillArea((x)*(CELL_SIZE_PX+PADDING_PX)+PALYA_START_X, (y)*(CELL_SIZE_PX+PADDING_PX)+PALYA_START_Y, CELL_SIZE_PX, CELL_SIZE_PX, (color)))
#define print_cell(s,x,y)({\
	switch((s).palya[(x)][(y)][type]){\
		case FAROK:\
			draw_cell((x), (y), SZIN_FAROK);\
			break;\
		case FEJ:\
			draw_cell((x), (y), SZIN_FEJ);\
			break;\
		case SIMA:\
			draw_cell((x), (y), SZIN_SIMA);\
			break;\
		case KAJA:\
			draw_cell((x), (y), SZIN_KAJA);\
			break;\
		case FAL:\
			draw_cell((x), (y), SZIN_FAL);\
			break;\
		default:\
			draw_cell((x), (y), SZIN_URES);\
			break;\
	}\
})

snake_t new_snake(unsigned char level){
	snake_t n;
	init_snake(&n, level);
	return n;
}

void init_snake(snake_t * n, unsigned char level){
	n->level = level;
	load_level(n);
	koord cx = n->fej[X]; koord cy = n->fej[Y];
	n->farok[X] = cx-1; n->farok[Y] = cy;
	n->hossz=2;
	n->palya[cx][cy][type] = FEJ;
	n->palya[cx][cy][p_irany] = balra;
	n->palya[cx-1][cy][type] = FAROK;
	n->palya[cx-1][cy][n_irany] = jobbra;
	n->palya[cx-1][cy][p_irany] = balra;
	n->menet = jobbra;
	n->n_ures = PALYA_HEIGHT*PALYA_WIDTH-n->hossz-n->n_ures;
	n->pont=0;
	insert_before_head(n);
}


void insert_before_head(snake_t * s){
	koord x = s->fej[X];
	koord y = s->fej[Y];
	s->fej[X] = (s->fej[X] + irany_matrix[s->menet][X])%PALYA_WIDTH;
	s->fej[Y] = (s->fej[Y] + irany_matrix[s->menet][Y])%PALYA_HEIGHT;
	s->palya[x][y][type] = SIMA;
	s->palya[x][y][n_irany] = s->menet;
	s->palya[s->fej[X]][s->fej[Y]][type] = FEJ;
	s->palya[s->fej[X]][s->fej[Y]][p_irany] = irany_inverz[s->menet];
	s->hossz++;
	s->n_ures--;
}

#define SZIN_FEJ COLOR_RED
#define SZIN_FAROK COLOR_GREEN
#define SZIN_SIMA COLOR_GREEN
#define SZIN_KAJA COLOR_ORANGE
#define SZIN_URES COLOR_WHITE
#define SZIN_FAL COLOR_BLACK

void print_snake(snake_t s){
	koord x, y;
	for(y=0; y<PALYA_HEIGHT; y++){
		for(x=0; x<PALYA_WIDTH; x++){
			print_cell(s, x, y);
		}
	}
}

void print_lepes(snake_t s){
	koord x, y;
	x = s.farok[X];
	y = s.farok[Y];
	draw_cell(x, y, SZIN_FAROK);
	print_cell(s, (x+irany_matrix[s.palya[x][y][p_irany]][X])%PALYA_WIDTH, (y+irany_matrix[s.palya[x][y][p_irany]][Y])%PALYA_HEIGHT);
	x = s.fej[X];
	y = s.fej[Y];
	draw_cell(x, y, SZIN_FEJ);
	print_cell(s, (x+irany_matrix[s.palya[x][y][p_irany]][X])%PALYA_WIDTH, (y+irany_matrix[s.palya[x][y][p_irany]][Y])%PALYA_HEIGHT);
}

/*void draw_cell(int x, int y, color_t color){
	fillArea(x*(CELL_SIZE_PX+PADDING_PX)+PALYA_START_X, y*(CELL_SIZE_PX+PADDING_PX)+PALYA_START_Y, CELL_SIZE_PX, CELL_SIZE_PX, color);
}

void print_cell(snake_t s, koord x, koord y){
	switch(s.palya[x][y][type]){
		case FAROK:
			draw_cell(x, y, SZIN_FAROK);
			break;
		case FEJ:
			draw_cell(x, y, SZIN_FEJ);
			break;
		case SIMA:
			draw_cell(x, y, SZIN_SIMA);
			break;
		case KAJA:
			draw_cell(x, y,SZIN_KAJA);
			break;
		default:
			draw_cell(x, y, SZIN_URES);
			break;
	}
}*/

void lep(snake_t * s){
	s->palya[s->fej[X]][s->fej[Y]][type] = SIMA;
	s->palya[s->fej[X]][s->fej[Y]][n_irany] = s->menet;
	s->fej[X] = (s->fej[X] + irany_matrix[s->menet][X])%PALYA_WIDTH;
	s->fej[Y] = (s->fej[Y] + irany_matrix[s->menet][Y])%PALYA_HEIGHT;
	s->palya[s->fej[X]][s->fej[Y]][p_irany] = irany_inverz[s->menet];
	s->palya[s->farok[X]][s->farok[Y]][type] = URES;
	s->palya[s->fej[X]][s->fej[Y]][type] = FEJ;
	koord x=s->farok[X];
	s->farok[X] = (s->farok[X] + irany_matrix[s->palya[s->farok[X]][s->farok[Y]][n_irany]][X])%PALYA_WIDTH;
	s->farok[Y] = (s->farok[Y] + irany_matrix[s->palya[x][s->farok[Y]][n_irany]][Y])%PALYA_HEIGHT;
	s->palya[s->farok[X]][s->farok[Y]][type] = FAROK;
}

void new_kaja(snake_t * s, koord * px, koord * py){
	int dobas = random(random(RTC_GetTicks()))%s->n_ures;
	koord x=*px;
	koord y=*py;
	for(y=0; y<PALYA_HEIGHT; y++){
		for(x=0; x<PALYA_WIDTH; x++){
			if(s->palya[x][y][type] == URES){
				dobas--;
				if(dobas<=0){
					s->palya[x][y][type] = KAJA;
					s->n_ures--;
					*px=x;
					*py=y;
					return;
				}
			}
		}
	}
}

esemeny detect_esemeny(snake_t s, irany i){
	if(irany_inverz[i] == s.menet){i=s.menet;}
	koord x, y;
	x = (s.fej[X] + irany_matrix[i][X])%PALYA_WIDTH;
	y = (s.fej[Y] + irany_matrix[i][Y])%PALYA_HEIGHT;
	switch(s.palya[x][y][type]){
		case FAL:
		case SIMA:
		case FEJ:
			return utkozes;
			break;
		case KAJA:	
			return kaja;
			break;
		default:
			return lepes;
			break;
	}
}

unsigned short random(int extra_seed){
	int seed = 0;
	int seed2 = 0;
	for(int i = 0; i < 32; i++ ){
		seed <<= 1;
		seed |= (RTC_GetTicks()%2);
	}
	for(int i = 0; i < 32; i++ ){
		seed2 <<= 1;
		seed2 |= (RTC_GetTicks()%16);
	}
	seed ^= seed2;
	seed = (( 0x41C64E6D*seed ) + 0x3039);
	seed2 = (( 0x1045924A*seed2 ) + 0x5023);
	extra_seed = (extra_seed)?(( 0xF201B35C*extra_seed ) + 0xD018):(extra_seed);
	seed ^= seed2;
	if(extra_seed){ seed ^= extra_seed; }
	return ((seed >> 16) ^ seed) >> 16;
} 
